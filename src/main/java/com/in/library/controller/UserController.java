package com.in.library.controller;

import com.in.library.service.UserService;
import com.in.library.model.dto.UserDto;
import com.in.library.model.entity.UserRegisterationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/get-all-users")
    public List<UserDto> getUsers() {
        return userService.findAllUsers();
    }

    @GetMapping("/get-user-name-by-id")
    public String getUserNameById(@RequestParam Integer userId){
        return userService.getUserById(userId).getUserName();
    }

    @DeleteMapping("/delet-user-by-id")
    public String deletUserById(@RequestParam  Integer userId) {

        return   userService.deletUserById(userId);
    }

    @PostMapping(path = "/save-user")
    public UserDto registerNewUser(@RequestBody UserRegisterationEntity userRegisterationEntity){
        return userService.saveUser(userRegisterationEntity);
    }

    @PutMapping(value = "/update-user")
    public UserDto updateUser(@RequestBody UserRegisterationEntity userRegisterationEntity){
        return  userService.updateUser(userRegisterationEntity);
    }

}
