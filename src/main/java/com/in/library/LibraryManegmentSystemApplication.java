package com.in.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryManegmentSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryManegmentSystemApplication.class, args);
	}

}
