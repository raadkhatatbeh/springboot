package com.in.library.model.repository;

import com.in.library.model.entity.UserRegisterationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepo extends JpaRepository<UserRegisterationEntity,Integer> {

}
