package com.in.library.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_books")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserBookEntity {

    @Transient
    private String bookName;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_book_id")
    private int userBookId;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "book_id")
    private int bookId;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id",insertable = false,updatable = false)
    private UserRegisterationEntity userRegisterationEntity;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id",insertable = false,updatable = false)
    private BookEntity bookEntity;

    public String getBookName() {
        return this.bookEntity.getBookName();
    }
}
