package com.in.library.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity(name = "user-registeration")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRegisterationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "fullname")
    private String fullName;

    @Column(name = "gender")
    private String gender;


    @JsonManagedReference
    @OneToOne(mappedBy = "userRegisterationEntity",cascade = CascadeType.ALL)
    private UserAdressEntity userAdressEntity;

    @JsonManagedReference
    @OneToMany(mappedBy = "userRegisterationEntity",cascade = CascadeType.ALL)
    private List<UserNoteEntity> userNoteEntity;

    @JsonManagedReference
    @OneToMany(mappedBy = "userRegisterationEntity",cascade = CascadeType.ALL)
    private List<UserBookEntity> userBookEntities;

}
