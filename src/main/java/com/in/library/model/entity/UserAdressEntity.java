package com.in.library.model.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_adress")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserAdressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adress_id")
    private int adressId;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "adress")
    private String adress;



    @JsonBackReference
    @JoinColumn(name = "user_id",insertable = false,updatable = false)
    @OneToOne(cascade = CascadeType.ALL)
    private UserRegisterationEntity userRegisterationEntity;


}
