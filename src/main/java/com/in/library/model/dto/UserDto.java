package com.in.library.model.dto;


import com.in.library.model.entity.UserAdressEntity;
import com.in.library.model.entity.UserBookEntity;
import com.in.library.model.entity.UserNoteEntity;
import com.in.library.model.entity.UserRegisterationEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private int userId;
    private String userName;
    private String password;
    private String fullName;
    private String gender;
   private UserAdressEntity userAdressEntity;
    private List<UserNoteEntity> userNoteEntity;
// private List<UserBookEntity> userBookEntity;

    public static UserDto toDto(UserRegisterationEntity userRegisterationEntity){
        return fillDataToUserDto(userRegisterationEntity);
    }
    public static UserDto fillDataToUserDto(UserRegisterationEntity userRegisterationEntity){
        UserDto userDto = new UserDto(userRegisterationEntity.getUserId(), userRegisterationEntity.getUserName(),
                                      userRegisterationEntity.getPassword(), userRegisterationEntity.getFullName(),
                                     userRegisterationEntity.getGender(),userRegisterationEntity.getUserAdressEntity(),
                                    userRegisterationEntity.getUserNoteEntity());//userRegisterationEntity.getUserBookEntities());
        return userDto;
    }

    public static List<UserDto> toDto(List<UserRegisterationEntity> userRegisterationEntity){
        return userRegisterationEntity.stream().map(x -> toDto(x)).collect(Collectors.toList());
    }
}
