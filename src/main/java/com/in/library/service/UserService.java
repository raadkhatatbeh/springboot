package com.in.library.service;

import com.in.library.model.dto.UserDto;
import com.in.library.model.entity.UserRegisterationEntity;
import com.in.library.model.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    public List<UserDto> findAllUsers(){

        return UserDto.toDto(userRepo.findAll());
    }

    public UserDto getUserById(Integer userId) {

        return UserDto.toDto(userRepo.getById(userId));
    }

    public String deletUserById(Integer userId){
        UserDto user = this.getUserById(userId);
        if (user.getUserId() == userId) {
            userRepo.deleteById(userId);
            return "User deleted!";
        }
        else {
            return "User Not exist";
        }
    }
    public UserDto saveUser(UserRegisterationEntity userRegisterationEntity){
        return UserDto.toDto(userRepo.save(userRegisterationEntity));

    }
    public UserDto updateUser(UserRegisterationEntity userRegisterationEntity){
        UserDto user = this.getUserById(userRegisterationEntity.getUserId());
        if (user.getUserId() == userRegisterationEntity.getUserId()){
            return UserDto.toDto(userRepo.save(userRegisterationEntity));
        }
        else
        {
            return null;
        }
    }


}
